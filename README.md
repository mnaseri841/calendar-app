# calendar_app

A prayer times app developed with #Flutter

## Getting Started

This app needs Internet and GeoLocation to function properly.

You can get precise prayer times such as Sunrise, Maghreb, Dhuhr etc.
exactly for your current location
